import sys
import os

sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('.'))

import vk_api
from src.vk.vk_session import VKSession

import json
from loguru import logger

import unittest


class VKTest(unittest.TestCase):
    def setUp(self):
        self.vk_session = VKSession().get_vk_session()

    def test_vk_post_wall(self):
        """ Пост записи на стену пользователя
        """
        tools = vk_api.VkTools(self.vk_session)
        vk_app = self.vk_session.get_api()
        post_result = vk_app.wall.post(message='.')

        logger.info("The post was successfully posted with post_id {}".format(post_result))

    def test_get_wall_posts(self):
        """ Получение всех постов со стены группы

            run test: python -m unittest vk_test.py.VKTest.test_get_wall_posts
        """
        group_id = -43938013

        tools = vk_api.VkTools(self.vk_session)
        wall = tools.get_all('wall.get', 100, {'owner_id': group_id})
        
        logger.info('Posts count: {}'.format(wall['count']))

        with open("output/wall_asp.txt", 'a') as f:
            f.write(json.dumps(wall))
            f.close()

if __name__ == '__main__':
    unittest.main()