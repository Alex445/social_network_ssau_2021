import vk_api
from loguru import logger

from src.utils import get_vk_data


def auth_handler():
    """ При двухфакторной аутентификации вызывается
        эта функция.
    """
    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")
    # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device
    
class VKSession:
    def __init__(self):
        self.__init_auth_data()

    def __init_auth_data(self):
        vk_metadata = get_vk_data()
        self.login, self.password = vk_metadata.login, vk_metadata.password

    def __auth_session(self):
        try:
            self.vk_session.auth()
            logger.info("VK authentication was successful")
            return self.vk_session
            
        except vk_api.AuthError as error_msg:
            logger.error("Error with {}".format(error_msg))
            raise vk_api.AuthError

    def get_vk_session(self):

        self.vk_session = vk_api.VkApi(
            self.login, self.password,
            auth_handler=auth_handler # функция для обработки двухфакторной аутентификации
        )

        return self.__auth_session()
