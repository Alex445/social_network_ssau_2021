import yaml

from data.vk_auth_data import VKAuthData
from data.twitter_auth_data import TwitterAuthData


def get_vk_data():
    with open('static/vk.yml') as f:
        templates = yaml.safe_load(f)

    return VKAuthData(templates['login'], templates['password'])

def get_twitter_data():
    with open('static/twitter.yml') as f:
        templates = yaml.safe_load(f)

    return TwitterAuthData(templates['login'], templates['password'])
