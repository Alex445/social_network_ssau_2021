from dataclasses import dataclass

@dataclass
class VKAuthData:
    login: str
    password: str

