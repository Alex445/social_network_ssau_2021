from dataclasses import dataclass

@dataclass
class TwitterAuthData:
    login: str
    password: str
