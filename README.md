# social_network_ssau_2021


## To set up the environment
```
python -m pip install -r requirements.txt
```

## To set up the data artifacts 
Put to ```static/vk.yml``` and ```static/twitter.yml``` your auth data (login and password) as mentioned in ```auth_template.yml```

## To check lab is completed
run ```test/vk_test.py``` and ```twitter/test.py```
